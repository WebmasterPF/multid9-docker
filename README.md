Multisite D9 sur Docker4Drupal - Test update des D8 vers D9, test migration D6 > D9

Lancer les conteneurs avec:
docker-compose up -d --remove-orphans && docker ps

Stopper tous les conteneurs avec:
docker stop $(docker ps -a -q) && docker ps

Supprimer / détruire tous les conteneurs avec:
docker rm $(docker ps -a -q) && docker ps

Accéder à un conteneur via le terminal
docker exec -it NOMCONTENEUR bash -c "mysql -u test -p root123"

Trouver l'IP assignée au conteneur par Docker
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' NOMCONTENEUR

Par ex:
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' multid8_mariadb